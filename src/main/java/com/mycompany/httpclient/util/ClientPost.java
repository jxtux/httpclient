package com.mycompany.httpclient.util;

import com.mycompany.httpclient.vto.UserVto;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectionRequest;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;

public class ClientPost {

    public void connectionPost(String url, String email, String pass) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            HttpPost httpPost = new HttpPost(url);

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("txtEmail", email));
            params.add(new BasicNameValuePair("txtPassword", pass));
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            CloseableHttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                System.out.println(line);
            }

        } catch (IOException e) {
            throw new IOException(e.getMessage());
        } finally {
            client.close();
        }
    }

    public void connectionPostJson(String url, String json) throws Exception {

        //DEFINIMOS TIEMPO DE ESPERA
        int timeout = 5;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000)
                .build();

        CloseableHttpClient client = HttpClientBuilder.create()
                .setDefaultRequestConfig(config)
                .build();

        try {
            HttpPost request = new HttpPost(url);

            StringEntity entity = new StringEntity(json, "UTF-8");
            request.setEntity(entity);
            request.setHeader(HttpHeaders.ACCEPT, "application/json");
            request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");

            CloseableHttpResponse response = client.execute(request);

            //STATUS
            System.out.println("HTTP Status of response: " + response.getStatusLine().getStatusCode());

            //HEADER
            Header[] headerV = response.getHeaders("Content-Type");
            System.out.println("header:" + headerV[0].getValue());

            Header[] headers2 = response.getAllHeaders();
            for (Header header : headers2) {
                System.out.println(header.getName() + ":" + header.getValue());
            }

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                System.out.println(line);
            }

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            client.close();
        }
    }

    public void connectionPost01Manager(String url, String email, String pass) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            HttpPost httpPost = new HttpPost(url);

//            BasicHttpClientConnectionManager connManager = new BasicHttpClientConnectionManager();
//            HttpRoute route = new HttpRoute(new HttpHost("www.baeldung.com", 80));
//            ConnectionRequest connRequest = connManager.requestConnection(route, null);
//            connRequest.

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("txtEmail", email));
            params.add(new BasicNameValuePair("txtPassword", pass));
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            CloseableHttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                System.out.println(line);
            }

        } catch (IOException e) {
            throw new IOException(e.getMessage());
        } finally {
            client.close();
        }
    }
}
