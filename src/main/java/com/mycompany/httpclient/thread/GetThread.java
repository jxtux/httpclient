package com.mycompany.httpclient.thread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.protocol.HttpContext;

public class GetThread extends Thread {

    private CloseableHttpAsyncClient client;
    private HttpContext context;
    private HttpGet request;

    public GetThread(CloseableHttpAsyncClient client, HttpGet req) {
        this.client = client;
        context = HttpClientContext.create();
        this.request = req;
    }

    @Override
    public void run() {
        try {
            Future<HttpResponse> future = client.execute(request, context, null);
            HttpResponse response = future.get();

            leerResponse(response);
        } catch (InterruptedException | ExecutionException ex) {
            System.out.println(ex.getLocalizedMessage());
        } catch (IOException ex) {
            Logger.getLogger(GetThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void leerResponse(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line = "";
        
        while ((line = rd.readLine()) != null) {
            System.out.println("asincrono thread:"+ line);
        }
    }
}
