
import com.google.gson.Gson;
import com.mycompany.httpclient.thread.GetThread;
import com.mycompany.httpclient.thread.MultiHttpClientConnThread;
import com.mycompany.httpclient.util.ClientAsincrono;
import com.mycompany.httpclient.util.ClientGet;
import com.mycompany.httpclient.util.ClientPost;
import com.mycompany.httpclient.vto.UserVto;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.impl.nio.conn.PoolingNHttpClientConnectionManager;
import org.apache.http.impl.nio.reactor.DefaultConnectingIOReactor;
import org.apache.http.nio.reactor.ConnectingIOReactor;
import org.apache.http.nio.reactor.IOReactorException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ClientHttpTest {

    public ClientHttpTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void ClientHttpAsincrono() {

        ClientAsincrono c = new ClientAsincrono();
        String urlA = "http://localhost:9000/public/listJob";

        try {
            System.out.println("asincrono........ ");
            c.asincronoGet(urlA);
        } catch (Exception ex) {
            Logger.getLogger(ClientHttpTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void ClientHttpSimple() {
        ClientGet g1 = new ClientGet();
        String urlG = "http://localhost:9000/public/listJob";

        ClientPost p1 = new ClientPost();
        String urlP = "http://localhost:9000/public/dataPostEnviada";

        String urlJ = "http://localhost:9000/public/dataPostEnviadaJson";
        UserVto u = new UserVto("joseantonio5x@gmail.com", "passXxxxx");
        Gson gson = new Gson();
        String json = gson.toJson(u);

        try {
            g1.connectionGet01(urlG);
            p1.connectionPost(urlP, "joseantonio5x@hotmail.com", "xxxx");
            p1.connectionPostJson(urlJ, json);
        } catch (Exception ex) {
            Logger.getLogger(ClientHttpTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void MultiHilo() {
        try {
            HttpGet get1 = new HttpGet("http://localhost:9000/public/name");
            HttpGet get2 = new HttpGet("http://localhost:9000/public/listJob");
            HttpGet get3 = new HttpGet("http://localhost:9000/public/listJob");

            PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
            connManager.setMaxTotal(5);//Conexiones totales abiertas por defecto 2
            connManager.setDefaultMaxPerRoute(4);//conexiones simultaneas maxima a una ruta por defecto es 2
            HttpHost host = new HttpHost("localhost:9000", 80);//conexiones simultaneas maxima a una ruta POR DEFECTO por defecto es 2
            connManager.setMaxPerRoute(new HttpRoute(host), 5);

            CloseableHttpClient client1 = HttpClients.custom().setConnectionManager(connManager).build();
            CloseableHttpClient client2 = HttpClients.custom().setConnectionManager(connManager).build();

            MultiHttpClientConnThread thread1 = new MultiHttpClientConnThread(client1, get1);
            MultiHttpClientConnThread thread2 = new MultiHttpClientConnThread(client2, get2);
            MultiHttpClientConnThread thread3 = new MultiHttpClientConnThread(client2, get3);
            thread1.start();
            thread2.start();
            thread3.start();
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
        }
    }

    @Test
    public void ClientHttpAsincronoThread() throws IOReactorException, InterruptedException {

        ConnectingIOReactor ioReactor = new DefaultConnectingIOReactor();
        PoolingNHttpClientConnectionManager cm = new PoolingNHttpClientConnectionManager(ioReactor);
        CloseableHttpAsyncClient client = HttpAsyncClients.custom().setConnectionManager(cm).build();
        client.start();

        String[] toGet = {
            "http://localhost:9000/public/listJob",
            "http://localhost:9000/public/name",
            "http://localhost:9000/public/name"
        };

        GetThread[] threads = new GetThread[toGet.length];
        for (int i = 0; i < threads.length; i++) {
            HttpGet request = new HttpGet(toGet[i]);
            threads[i] = new GetThread(client, request);
        }

        for (GetThread thread : threads) {
            thread.start();
        }
        for (GetThread thread : threads) {
            thread.join();
        }
    }

}
